import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { F1RacerManagementService} from "./f1-racer-management.service";
import { AppComponent } from './app.component';
import { F1RacerDetailsComponent } from './f1-racer-details/f1-racer-details.component';
import { FormsModule} from "@angular/forms";
import {Alert} from "selenium-webdriver";
import {AlertModule} from "ngx-bootstrap";
import {FilterPipe} from "./filter.pipe";

@NgModule({
  declarations: [
    AppComponent,
    F1RacerDetailsComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AlertModule.forRoot()
  ],
  providers: [F1RacerManagementService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
